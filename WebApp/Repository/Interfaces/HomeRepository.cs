﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Repository.Interfaces
{
    public interface HomeRepository
    {
        string GetMessage();
    }
}
