﻿using System;
using System.Collections.Generic;

namespace WebApp.Models
{
    public partial class Classroom
    {
        public Classroom()
        {
            Student = new HashSet<Student>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Khoavien { get; set; }

        public ICollection<Student> Student { get; set; }
    }
}
