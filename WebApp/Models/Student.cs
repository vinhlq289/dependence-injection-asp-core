﻿using System;
using System.Collections.Generic;

namespace WebApp.Models
{
    public partial class Student
    {
        public int Id { get; set; }
        public string Mssv { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public int? IdLop { get; set; }

        public Classroom IdLopNavigation { get; set; }
    }
}
